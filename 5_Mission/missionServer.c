modded class MissionServer
{
	ref array<string> m_NullArray = new array<string>;
	string m_NullString = "";
    string m_StringConfig;
    int m_IntConfig;
    string m_StringFirstLineFromArray;
	
	void MissionServer()
	{
		Print("[dzr_notes] ::: Starting Serverside");
		#ifdef AMS_AdditionalMedicSupplies
			Print("[dzr_notes] ::: Detected Additional Medic Supplies. Applying compatibility patch.");
		#endif
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionServer_SendFileToClient", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionServer_LogNote", this, SingleplayerExecutionType.Both );
		DZR_MissionServer_CheckFolders();
		
		ref array<string> m_CfgArray = new array<string>;
		
		//type1
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes/static_notes/type1", "NoteNameInText1.txt", m_NullArray, "Spawn a static note type 1 (DZR_PaperWrittenAdmin_Static, DZR_PaperWrittenAdmin_Red_Static, DZR_PaperWrittenAdmin_Green_Static, DZR_PaperWrittenAdmin_Blue_Static) and write this text file name in it without .txt part. When someone tries to read that note, the note will show the text from the file.\r\nSo can place 10 notes in ten location and set one text source for all of them. Anytime you change this text in the file, it will be updated in all note in game.\r\n\r\nСпавним, например статичную записку первого типа (DZR_PaperWrittenAdmin_Static, DZR_PaperWrittenAdmin_Red_Static, DZR_PaperWrittenAdmin_Green_Static, DZR_PaperWrittenAdmin_Blue_Static), но в текст пишем имя файла без .txt и при прочтении в записке будет текст из файла вместо его имени.\r\nТак можно разместить 10 записок в разных локациях и задать всем одинаковый текст. Так же текст можно сразу у всех отредактировать в файле и он сразу заменится в игре.");
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes/static_notes/type1", "NoteNameInText2.txt", m_NullArray, "Same here. If any static note type 1 has NoteNameInText2 it will show the this text. You can spawn plain static note, red, green, and blue.\r\n\r\nЭто второй текст-заготовка. Если указать в записках NoteNameInText2 вместо текста, то будет показан этот текст из файла.");
		
		//type 2
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes/static_notes/type2", "UnlimitedText.html", m_NullArray, "<html>Not implemented yet.<hr>Пока не разработано.</html>");
		
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "README_EN_v1.txt", m_NullArray, "How to give a note with your text to the starting gear?\r\n\r\nAdd this code to your init.c into the StartingEquipSetup class after SetRandomHealth( itemEnt );:\r\n\r\n			itemEnt = player.GetInventory().CreateInInventory( \"DZR_PaperWrittenAdmin_Red\" );\r\n			DZR_PaperWrittenAdmin_Red red_note = DZR_PaperWrittenAdmin_Red.Cast( itemEnt );\r\n			WrittenNoteData m_NoteContents = new WrittenNoteData(red_note);\r\n			m_NoteContents.SetNoteText(\"Here goes your text\\r\\nAnd the second line. Use \\r\\n to start new lines of thext.\");\r\n			red_note.SetPaperWrittenText(m_NoteContents);");
		
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "README_RU_v1.txt", m_NullArray, "Как выдать записку игроку в стартовом снаряжении??\r\n\r\nДобавьте этот код в ваш init.c в класс StartingEquipSetup после SetRandomHealth( itemEnt );:\r\n\r\n			itemEnt = player.GetInventory().CreateInInventory( \"DZR_PaperWrittenAdmin_Red\" );\r\n			DZR_PaperWrittenAdmin_Red red_note = DZR_PaperWrittenAdmin_Red.Cast( itemEnt );\r\n			WrittenNoteData m_NoteContents = new WrittenNoteData(red_note);\r\n			m_NoteContents.SetNoteText(\"Тут ваш текст.\\r\\nА это вторая строка.\\r\\nНовые строки разделяются \\r\\n\");\r\n			red_note.SetPaperWrittenText(m_NoteContents);");
		
		
		
		
		//m_IntConfig = dzr_notes_cfg_CFG.GetFile("$profile:\\DZR\\dzr_notes", "IntConfig.txt", m_NullArray, "1").ToInt();
		//m_StringFirstLineFromArray = dzr_notes_cfg_CFG.GetFile("$profile:\\DZR\\dzr_notes", "ArrayConfig.txt", m_CfgArray, "Line1\r\nLine2");
		
		
	}
	
	void DZR_MissionServer_LogNote(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
	{
		Print("[dzr_notes] ::: Got RPC from client");
		ref Param1< string > data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if (data.param1 != "")
            {
				
                string steam_id = sender.GetPlainId();
				string player_name = sender.GetName();
				DZR_MissionServer_CheckFolders();
				PlayerBase thePlayer = dzr_notes_cfg_CFG.dzrGetPlayerByIdentity(sender);
				vector plr_pos = thePlayer.GetPosition();
				SaveServerSide(player_name, steam_id, data.param1, plr_pos[0].ToString()+" "+plr_pos[1].ToString()+" "+plr_pos[2].ToString());
				//Print("DZR_MissionServer_LogNote(data.param1, sender);:" +data.param1);
				
			}
		}
	}
	
	void DZR_MissionServer_CheckFolders()
	{
		string dzr_notes_ProfileFolder = "$profile:\\";
		string dzr_notes_TagFolder = "DZR\\";
		string dzr_notes_ModFolder = "dzr_notes\\";
		string dzr_notes_SubFolder = "log\\";
		
		//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(dzr_notes_ProfileFolder+dzr_notes_TagFolder)){
			//NO DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO DZR");
			MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder);	
			MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder);	
			//dzr_writeConfigFile();			
		}
		else 
		{
			//YES DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile YES DZR");
			if (!FileExist(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder)){
				//NO IDZ
				//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO IDZ");
				
				MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder);
				
				
				//dzr_writeConfigFile();
			}
			else 
			{
				if (!FileExist(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder)){
					MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder);
				}
			}
			
		};
		
		
		
		//PROPER CONFIG
		
	}
	
	void DZR_MissionServer_SendFileToClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
	{
		Print("[dzr_notes] ::: Got RPC from client");
		ref Param1< string > data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if (data.param1 != "")
            {
				
                //bool result =  data.param1.ToggleShowNameToAll();
				Print("SendFileToClient(data.param1, sender);:" +data.param1);
				SendFileToClient(data.param1, sender);
				
			}
		}
	}
	
	void SendFileToClient(string m_TxtFileName, PlayerIdentity identity)
	{
		ref array<string> compressed = new array<string>;
		DZRnotes_JsonSaveData packed_text_object = new DZRnotes_JsonSaveData();
		
		// READ FILE CONTENTS
		string content;
		FileHandle fhandle;
		if (FileExist(m_TxtFileName))
		{
			//Print("DZR_PaperWritten_-14782089181514317906688471483907209516.json EXISTS!");
			fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				content += line_content;
			}
			CloseFile(fhandle);
		}
		else
		{
			//Print("NO FILE! "+ m_ObjectFileName);
		}
		// READ FILE CONTENTS
		
		compressed = DZR_Notes().PackText(content);
		packed_text_object.text = compressed;
		
		
		
		//save it
		//string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
		//JsonFileLoader< DZRnotes_JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
		//JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
		//save it
		
		//ref Param1<DZRnotes_JsonSaveData> m_Data = new Param1<DZRnotes_JsonSaveData>(packed_text_object);
		//GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
		//Print("[dzr_notes] ::: Sent RPC to client");
	}
	
	
	void SaveServerSide(string PlayerName, string steam_id, string Notetext, string ps)
	{
		int hour;
		int minute;
		int second;
		int year;
		int month;
		int day;
		GetHourMinuteSecond(hour, minute, second);
		GetYearMonthDay(year, month, day);
		string suffix = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"."+minute.ToString()+"."+second.ToString();
		string m_ObjectFileName = "$profile:DZR/dzr_notes/log/"+steam_id+"_"+PlayerName+"_"+suffix+".txt";
		FileHandle fhandle;
		if ( !FileExist(m_ObjectFileName) )
		{
			
			FileSerializer file = new FileSerializer;
			//FileHandle file = OpenFile(m_ObjectFileName, FileMode.APPEND);
			if (file.Open( m_ObjectFileName, FileMode.WRITE))
			{
				file.Write(Notetext); //ps
				//FPrintln(file, Notetext);
				//FPrintln(file, ps);
				file.Close();
				Print("missionServer.c ::: "+PlayerName + " ("+steam_id+") wrote text: " +Notetext+ " (logged to file: "+m_ObjectFileName+")");
			}
			else
			{
				Print("Failed write "+m_ObjectFileName);
			}
			
			
		} 
	}
	
}