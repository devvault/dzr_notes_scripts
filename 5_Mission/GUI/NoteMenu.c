modded class NoteMenu extends UIScriptedMenu
{
	
	void ~NoteMenu()
	{
		MissionGameplay mission = MissionGameplay.Cast( GetGame().GetMission() );
		if( mission )
		{
			IngameHud hud = IngameHud.Cast( mission.GetHud() );
			if ( hud )
			{
				hud.ShowHudUI( true );
			}
					GetGame().GetMission().RemoveActiveInputExcludes({"inventory"},false);
					GetGame().GetMission().RemoveActiveInputRestriction(EInputRestrictors.INVENTORY);
		}
	}
	
	override Widget Init()
	{
		//Print("[dzr_notes] ::: Started");
		//GetGame().Chat("[dzr_notes] ::: Widget Init", "colorAction");
		layoutRoot = GetGame().GetWorkspace().CreateWidgets("dzr_notes/gui/layout/dzr_inventory_note.layout");
		m_edit = MultilineEditBoxWidget.Cast( layoutRoot.FindAnyWidget("dzrTextInput") );
		m_html = HtmlWidget.Cast( layoutRoot.FindAnyWidget("dzrTextView") );
		m_confirm_button = ButtonWidget.Cast( layoutRoot.FindAnyWidgetById(IDC_OK) );
		
		return layoutRoot;
	}
	
	
	
	void InitNoteRead2(DZRnotes_JsonSaveData SaveData = null)
	{
		string text = DZR_Notes().UnpackText(SaveData.text);
		m_IsWriting = false;
		
		if (m_edit)
		{
			m_edit.Show(false);
		}
		
		if (m_html)
		{
			//TODO add text formating here. Just text string for now
			if (text)
			{
				m_html.Show(true);
				m_html.SetText(text);
				m_SymbolCount = text.Length(); //string.LengthUtf8() ?
				//m_html.SetText("<html><body><p>" + text + "</p></body></html>");
			}
		}
		m_confirm_button.Show(false);
	}
	/*
	override void Update( float timeslice )
	{
		super.Update( timeslice );
		
		if( GetGame() && GetGame().GetInput() && GetGame().GetInput().LocalPress("UAUIBack", false) )
		{
					GetGame().GetMission().RemoveActiveInputExcludes({"inventory"},false);
					GetGame().GetMission().RemoveActiveInputRestriction(EInputRestrictors.INVENTORY);
		}
	}
	*/
}
