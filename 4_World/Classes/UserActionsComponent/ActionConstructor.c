modded class ActionConstructor
{
    override void RegisterActions(TTypenameArray actions)
    {
        actions.Insert(ActionDzrLockItem);
		actions.Insert(ActionReadNoteOnGround);
        super.RegisterActions(actions);
        actions.Insert(ActionReadPaper);     
        actions.Insert(ActionWritePaper);
    }
}