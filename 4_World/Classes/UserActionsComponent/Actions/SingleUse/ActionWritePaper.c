modded class ActionWritePaper
{
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		Paper ItemPaper = Paper.Cast(item);
		Pen_ColorBase ItemPen = Pen_ColorBase.Cast(item);
		//Print(ItemPaper.GetType()+": "+ItemPaper.IsReadOnly());
		
		if ( ItemPaper && ItemPaper.IsReadOnly() && !ItemPen.IsKindOf("DZR_AdminPen") )
		{
			return false;
		};
		
		if ( ItemPen )
		{
			ItemPaper = Paper.Cast(target.GetObject());
			if(ItemPaper && ItemPaper.IsReadOnly() && !ItemPen.IsKindOf("DZR_AdminPen"))
			{
				return false;
			}
		};
		return super.ActionCondition(  player,  target,  item );
	}
	
};

