modded class ActionReadPaper
{
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		Paper target_object = Paper.Cast(target.GetObject());
			if (player.IsPlacingLocal())
			return false;
		
		if (player.IsPlacingLocal() && target_object && !target_object.IsReadable())
		return false;
		#ifdef AMS_AdditionalMedicSupplies
			Print("FIXING AMS for: "+item );
			if ( item && item.IsKindOf( "AMS_TestKitReport" ) )
			{
				return false;
			};
		#endif
		
		return true;
	};	
	
};


modded class ActionReadPaperCB
{
	override void OnStateChange(int pOldState, int pCurrentState)
	{
		if (pCurrentState == STATE_NONE && (!GetGame().IsDedicatedServer()))
		{
			if (GetGame().GetUIManager() && GetGame().GetUIManager().IsMenuOpen(MENU_NOTE))
				GetGame().GetUIManager().FindMenu(MENU_NOTE).Close();
		}
		else if (pCurrentState == STATE_LOOP_LOOP && pOldState != STATE_LOOP_LOOP && (!GetGame().IsMultiplayer() || GetGame().IsServer()))
		{
			ItemBase paper_item = ItemBase.Cast(m_ActionData.m_MainItem);
			
			//checking static notes
			string theFinalText = dzr_notes_cfg_CFG.GetNoteFromFile( paper_item );
			Param1<string> text = new Param1<string>(theFinalText);
			//checking static notes
			
			paper_item.RPCSingleParam(ERPCs.RPC_READ_NOTE, text, true,m_ActionData.m_Player.GetIdentity());
		}
	}
};

/*
modded class ActionReadPaperCB : ActionContinuousBaseCB
{
	override void OnStateChange(int pOldState, int pCurrentState)
	{
		if (pCurrentState == STATE_NONE && (!GetGame().IsDedicatedServer()))
		{
			if (GetGame().GetUIManager() && GetGame().GetUIManager().IsMenuOpen(MENU_NOTE))
				GetGame().GetUIManager().FindMenu(MENU_NOTE).Close();
		}
		else if (pCurrentState == STATE_LOOP_LOOP && pOldState != STATE_LOOP_LOOP && (!GetGame().IsMultiplayer() || GetGame().IsServer()))
		{
			ItemBase paper_item = ItemBase.Cast(m_ActionData.m_MainItem);
			
			//Param1<string> param_send = new Param1<string>("$profile:DZR/dzr_notes/DZR_PaperWritten_-14782089181514317906688471483907209516.txt");
			//GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionServer_SendFileToClient", param_send, true);
			DZRnotes_JsonSaveData FileData = new DZRnotes_JsonSaveData();
			FileData = SendFileToClient("$profile:DZR/dzr_notes/DZR_PaperWritten_-14782089181514317906688471483907209516.txt");
			
			Param1<ref DZRnotes_JsonSaveData> text = new Param1<ref DZRnotes_JsonSaveData>(FileData);
			Print("OnStateChange FileData.text[0]:" + FileData.text[0]);
			Print("OnStateChange FileData.text:" + FileData.text);
			Print("OnStateChange FileData:" + FileData);
			//Param1<DZRnotes_JsonSaveData> text = new Param1<DZRnotes_JsonSaveData>(paper_item.GetWrittenNoteData().GetNoteText());
			paper_item.RPCSingleParam(ERPCs.RPC_READ_NOTE, text, true,m_ActionData.m_Player.GetIdentity());
		}
	}
	
	DZRnotes_JsonSaveData SendFileToClient(string m_TxtFileName)
	{
		ref array<string> compressed = new array<string>;
		DZRnotes_JsonSaveData packed_text_object = new DZRnotes_JsonSaveData();

		// READ FILE CONTENTS
		string content;
		FileHandle fhandle;
		if (FileExist(m_TxtFileName))
		{
			//Print("DZR_PaperWritten_-14782089181514317906688471483907209516.json EXISTS!");
			fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				content += line_content;
			}
			CloseFile(fhandle);
		}
		else
		{
			//Print("NO FILE! "+ m_ObjectFileName);
		}
		// READ FILE CONTENTS

		compressed = DZR_Notes().PackText(content);
		packed_text_object.text = compressed;
		

		
		//save it
		//string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
		//JsonFileLoader< DZRnotes_JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
		//JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
		//save it
		
	return packed_text_object;
		//ref Param1<DZRnotes_JsonSaveData> m_Data = new Param1<DZRnotes_JsonSaveData>(packed_text_object);
		//GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
		//Print("[dzr_notes] ::: Sent RPC to client");
	}
};
*/