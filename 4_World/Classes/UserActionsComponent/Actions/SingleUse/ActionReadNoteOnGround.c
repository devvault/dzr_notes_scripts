class ActionReadNoteOnGround: ActionInteractBase
{
	/*
		#ifdef AMS_AdditionalMedicSupplies
		protected ref AMS_TestKitReportMenu m_Report1;
		#endif
	*/
	void ActionReadNoteOnGround()
	{
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_INTERACTONCE;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
	}
	
	override string GetText()
	{
		return "#read";
	}
	
	override bool ActionCondition ( PlayerBase player, ActionTarget target, ItemBase item )
	{
		Paper target_object = Paper.Cast(target.GetObject());
		
		if (player.IsPlacingLocal())
		return false;
		
		if ( player && target_object && target_object.IsReadable() )
		{
			return true;
		}
		
		return false;
	}
	
    override void OnStart( ActionData action_data )
    {
        super.OnStart( action_data );
		/*
			#ifdef AMS_AdditionalMedicSupplies
			m_Report1 = null;
			AMS_TestKitReport report = AMS_TestKitReport.Cast( action_data.m_Target.GetObject() );
			Print("OnStart AMS");
			if ( report && !m_Report1 )
			{
			if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
			{
			Print("AMS success, open Report");
			m_Report1 = new AMS_TestKitReportMenu( report );
			GetGame().GetUIManager().ShowScriptedMenu( m_Report1, NULL );
			}
			
			}
			#endif
		*/
		if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
		{	
			if (GetGame().GetUIManager() && GetGame().GetUIManager().IsMenuOpen(MENU_NOTE)) 
			{
				GetGame().GetUIManager().FindMenu(MENU_NOTE).Close();
			}
		}
	}
	
	override void OnStartServer( ActionData action_data )
	{
		ItemBase paper_item = ItemBase.Cast(action_data.m_Target.GetObject());
		bool isAMS = false;
		/*
			#ifdef AMS_AdditionalMedicSupplies
			Print(":::AMS detected:::");
			AMS_TestKitReport report = AMS_TestKitReport.Cast( action_data.m_Target.GetObject() );
			if(report)
			{
			isAMS = true;
			Print("AMS target is report" + report + " - " + m_Report1);
			}
			#endif
		*/
		if(paper_item && !isAMS)
		{
			//Print("No AMS opennote");
			
			//checking static notes
			string theFinalText = dzr_notes_cfg_CFG.GetNoteFromFile( paper_item );
			Param1<string> text = new Param1<string>(theFinalText);
			//checking static notes
			
			paper_item.RPCSingleParam(ERPCs.RPC_READ_NOTE, text, true,action_data.m_Player.GetIdentity());
		}
		/*
			#ifdef AMS_AdditionalMedicSupplies	
			Print("Checking report");
			if ( report && !m_Report1 )
			{
			Print("Reports true");
			if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
			{
			Print("AMS success, open Report");
			m_Report1 = new AMS_TestKitReportMenu( report );
			GetGame().GetUIManager().ShowScriptedMenu( m_Report1, NULL );
			
			}
			}
			else
			{
			Print("report && !m_Report1 are false");
			}
			
			#endif
		*/		
	}
}			