modded class ActionDeployObject
{
	
    override void SetupAnimation(ItemBase item)
    {
        if (item.IsKindOf("Paper"))
        {
            m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_INTERACT;
            return;
		}
		
        super.SetupAnimation(item);
	}
	
    override bool ActionCondition(PlayerBase player, ActionTarget target, ItemBase item)
    {
        if (!GetGame().IsMultiplayer() || GetGame().IsClient())
        {
            if (player.IsPlacingLocal() && item.IsKindOf("Paper"))
			{
				return true;
			}
		}
		
        return super.ActionCondition(player, target, item);
	}
	
	override bool ActionConditionContinue( ActionData action_data )
	{
		if (action_data.m_MainItem.IsKindOf("Paper"))
		{
			return true;
		};
		
		
        return super.ActionConditionContinue(action_data);
	}
	
	override void OnFinishProgressServer( ActionData action_data ) //FULL VANILLA COPY
	{	
		PlaceObjectActionData poActionData;
		poActionData = PlaceObjectActionData.Cast(action_data);
		
		if ( !poActionData ) { return; }
		if ( !action_data.m_MainItem ) { return; }
		
		EntityAI entity_for_placing = action_data.m_MainItem;
		vector position;
		vector orientation;
		
		// In case of placement with hologram
		if ( action_data.m_Player.GetHologramServer() )
		{
			position = action_data.m_Player.GetLocalProjectionPosition();
			orientation = action_data.m_Player.GetLocalProjectionOrientation();
			
			action_data.m_Player.GetHologramServer().EvaluateCollision(action_data.m_MainItem);
			if ( GetGame().IsMultiplayer() && action_data.m_Player.GetHologramServer().IsColliding() )
			{
				//MODDED PART
				if (!action_data.m_MainItem.IsKindOf("Paper"))
				{
					return;
				}
				//MODDDED PART
			}
			
			action_data.m_Player.GetHologramServer().PlaceEntity( entity_for_placing );
			
			if ( GetGame().IsMultiplayer() )
				action_data.m_Player.GetHologramServer().CheckPowerSource();
		}
		else
		{
			position = action_data.m_Player.GetPosition();
			orientation = action_data.m_Player.GetOrientation();
		
			position = position + ( action_data.m_Player.GetDirection() * POSITION_OFFSET );
		}
		
		action_data.m_Player.PlacingCompleteServer();
		entity_for_placing.OnPlacementComplete( action_data.m_Player, position, orientation );
		
		MoveEntityToFinalPosition( action_data, position, orientation );		
		GetGame().ClearJuncture( action_data.m_Player, entity_for_placing );
		action_data.m_MainItem.SetIsBeingPlaced( false );
		action_data.m_Player.GetSoftSkillsManager().AddSpecialty( m_SpecialtyWeight );
		poActionData.m_AlreadyPlaced = true;	
		action_data.m_MainItem.SoundSynchRemoteReset();
	}
}