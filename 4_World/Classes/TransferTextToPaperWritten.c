class TransferTextToPaperWritten : ReplaceItemWithNewLambdaBase
{
    protected ref WrittenNoteData m_NoteContents;

    void TransferTextToPaperWritten(EntityAI old_item, string new_item_type, ref WrittenNoteData data)
    {
        m_NoteContents = data;
    }

	override void CopyOldPropertiesToNew(notnull EntityAI old_item, EntityAI new_item)
	{
		super.CopyOldPropertiesToNew(old_item, new_item);
		DZR_PaperWritten note = DZR_PaperWritten.Cast(new_item);
		note.SetPaperWrittenText(m_NoteContents);
        //m_NoteContents = "";
	}
}