modded class WrittenNoteData
{
	protected ItemBase 	m_WritingImplement;
	protected ItemBase	m_Paper;
	protected PlayerBase	m_Player;
	//protected int 		m_Handwriting = -1;
	protected string 	m_SimpleText;
	protected string 	m_TextFromFile;// = "default";
	protected DZRnotes_JsonSaveData 	m_JsonTextFromFile;// = "default";
	
	void WrittenNoteData(ItemBase parent)
	{
		//GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", this, SingleplayerExecutionType.Both );
	}
	
	void DZR_MissionGameplay_GetFileFromServer2(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
	{
		Print("[dzr_notes] ::: Got RPC from server");
		Param1<DZRnotes_JsonSaveData> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
			m_TextFromFile = DZR_Notes().UnpackText(data.param1.text);
			Print("[dzr_notes] ::: data.param1.text: "+data.param1.text);
			Print("[dzr_notes] ::: data.param1.text[0]: "+data.param1.text[0]);
			Print("[dzr_notes] ::: data.param1.text[1]: "+data.param1.text[1]);
			Print("[dzr_notes] ::: data.param1.text[2]: "+data.param1.text[2]);
			Print("[dzr_notes] ::: m_TextFromFile: "+m_TextFromFile);
			//Print("[dzr_notes] ::: DZR_Notes().UnpackText(data.param1.text): "+DZR_Notes().UnpackText(data.param1.text));
			//Print("[dzr_notes] ::: DZR_Notes().UnpackText(data.param1.text[0]): "+DZR_Notes().UnpackText(data.param1.text[0]));
			//DZR_NOTES_SaveToClientJson(data.param1);
			string m_ObjectFileName = "$profile:DZR/dzr_notes/json_from_server2.json";
			JsonFileLoader<DZRnotes_JsonSaveData>.JsonSaveFile(m_ObjectFileName, data.param1);
		}
	}
	
	override void OnRPC( PlayerIdentity sender, int rpc_type, ParamsReadContext  ctx)
	{
		Param1<string> param = new Param1<string>("");
		//m_Player = dzr_notes_cfg_CFG.dzrGetPlayerByIdentity(sender);
		//sent from server, executed on client
		if (rpc_type == ERPCs.RPC_WRITE_NOTE)
		{
			m_Player = dzr_notes_cfg_CFG.dzrGetPlayerByIdentity(sender);
			Param1<string> param1 = new Param1<string>("");
			if (ctx.Read(param1))
			{
				SetNoteText(param1.param1);
			}
			
			g_Game.GetMission().SetNoteMenu(g_Game.GetUIManager().EnterScriptedMenu(MENU_NOTE, GetGame().GetUIManager().GetMenu())); //NULL means no parent
			
			ItemBase pen;
			ItemBase paper;
			//int handwriting;
			
			if (GetNoteInfo(pen,paper))
			{
				g_Game.GetMission().GetNoteMenu().InitNoteWrite(paper,pen,m_SimpleText);
				//g_Game.GetMission().GetNoteMenu().InitNoteWrite(paper,pen,m_TextFromFile);
				
				
			}
		}
		//sent from client (NoteMenu), executed on server
		if (rpc_type == ERPCs.RPC_WRITE_NOTE_CLIENT)
		{
			m_Player = dzr_notes_cfg_CFG.dzrGetPlayerByIdentity(sender);
			Param1<string> param2 = new Param1<string>("");
			if (ctx.Read(param2))
			{
				
				
				
				SetNoteText(param2.param1);
				if (GetNoteInfo(pen, paper))
				{
					
					TransferTextToPaperWritten lambda;
					
					Pen_ColorBase thePen = Pen_ColorBase.Cast(pen);
					
					if(thePen)
					{

						
						lambda = new TransferTextToPaperWritten(paper, thePen.Result(), this);
						Print(this);
						Print(thePen.Result()+" ("+thePen.GetType()+")");
					}
					
					PlayerBase player = dzrGetPlayerByIdentity(sender);
					
					if (player.GetItemInHands() == paper)
					{
						player.ServerReplaceItemInHandsWithNew(lambda);
					}
					else
					{
						player.ServerReplaceItemWithNew(lambda);
					}
				}
			}
		}
		if (rpc_type == ERPCs.RPC_READ_NOTE)
		{
			
			
			/*
				Param1<ref DZRnotes_JsonSaveData> param3;// = new Param1<ref DZRnotes_JsonSaveData>();
				
				if (ctx.Read(param3))
				{
				Print("param3: " + param3);
				Print("param3.param1: " + param3.param1);
				Print("param3.param1.text: " + param3.param1.text);
				Print("param3.param1.text[0]: " + param3.param1.text[0]);
				m_JsonTextFromFile = param3.param1;
				m_TextFromFile = DZR_Notes().UnpackText(param3.param1.text);
				SetNoteText(m_TextFromFile);
				//SetNoteText(param3.param1);
				string m_ObjectFileName = "$profile:DZR/dzr_notes/json_from_server2.json";
				JsonFileLoader<DZRnotes_JsonSaveData>.JsonSaveFile(m_ObjectFileName, param3.param1);
				}
			*/
			if (ctx.Read(param))
			{
				SetNoteText(param.param1);
			}
			
			g_Game.GetMission().SetNoteMenu(g_Game.GetUIManager().EnterScriptedMenu(MENU_NOTE, GetGame().GetUIManager().GetMenu())); //NULL means no parent
			g_Game.GetMission().GetNoteMenu().InitNoteRead(m_SimpleText);
		}
	}
	
	PlayerBase dzrGetPlayerByIdentity(PlayerIdentity identity)
	{
		if(identity)
		{
			int high, low;
			if (!GetGame().IsMultiplayer())
			{
				return PlayerBase.Cast(GetGame().GetPlayer());
			}
			
			GetGame().GetPlayerNetworkIDByIdentityID(identity.GetPlayerId(), low, high);
			return PlayerBase.Cast(GetGame().GetObjectByNetworkId(low, high));
		}
		return NULL;
	}
	
	override void InitNoteInfo(ItemBase pen = null, ItemBase paper = null)
	{
		m_WritingImplement = pen;
		m_Paper = paper;
		//m_Handwriting = handwriting;
	}
	
	override bool GetNoteInfo(out ItemBase pen, out ItemBase paper)
	{
		pen = m_WritingImplement;
		paper = m_Paper;
		//handwriting = m_Handwriting;
		return pen && paper;
	}
	
	override string GetNoteText()
	{		
		return m_SimpleText;
		//return m_TextFromFile;
	}
	
	PlayerBase GetPlayer()
	{
		return m_Player;
		//return m_TextFromFile;
	}
	
	override void SetNoteText(string text)
	{
		m_SimpleText = MiscGameplayFunctions.SanitizeString(text);
		//m_TextFromFile = MiscGameplayFunctions.SanitizeString(text);
	}
	
	override void DepleteWritingImplement(notnull ItemBase pen,string old_text,string new_text)
	{
		float qty_per_char = 1.0;
		float decrease = Math.Clamp((new_text.Length() - old_text.Length()),0,pen.GetQuantityMax());
		pen.AddQuantity(-(qty_per_char * decrease));
	}
}
