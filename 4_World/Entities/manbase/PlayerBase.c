modded class PlayerBase
{
	//protected bool m_SFSearchSound;
	protected bool m_DZRnotes_DuctSound;

	void PlayerBase()
	{
		RegisterNetSyncVariableBool("m_DZRnotes_DuctSound");
	}
	
	override void OnVariablesSynchronized()
	{
		super.OnVariablesSynchronized();		
		if (m_DZRnotes_DuctSound)
		{
			DZRDuctSoundItemSoundPlay();
		}
				
    }
	
	void DZRDuctSoundItemSoundPlay()
	{
        //int soundVariation = Math.RandomInt(5,8);
		EffectSound m_DZRDuctSound = SEffectManager.PlaySoundOnObject( "ducttape_tieup_SoundSet", this );
		//Print ("SOUND WHISTLE");
        m_DZRDuctSound.SetSoundVolume(2.0);
		m_DZRDuctSound.SetSoundAutodestroy( true );
		ResetDZRDuctSound();
	}
	
	void StartDZRDuctSound()
	{
		m_DZRnotes_DuctSound = true;
		SetSynchDirty();	
	}
	
	void ResetDZRDuctSound()
	{
		m_DZRnotes_DuctSound = false;
		SetSynchDirty();
	}
	
}