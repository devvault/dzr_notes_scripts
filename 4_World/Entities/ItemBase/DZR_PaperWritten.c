class DZR_PaperWritten extends Paper
{	
	protected bool m_DZR_IsLocked;
	//protected string m_DZR_ItemNote;
	
    void DZR_PaperWritten()
	{
        RegisterNetSyncVariableBool("m_DZR_IsLocked");
        //RegisterNetSyncVariableBool("m_DZR_ItemNote");
       // m_DZR_IsLocked = false;
		// m_DZR_ItemNote = "";
	}
	
	void SetPaperWrittenText(WrittenNoteData data)
	{
		m_NoteContents = data;
		// send server RPC
		
		//LOG NOTE
		/*
			ref Param1<string> m_Data = new Param1<string>(param1.param1);
			PlayerIdentity identity = sender;
			GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionServer_LogNote", m_Data, true, identity);
		*/
		//LOG NOTE
		DZR_MissionServer_CheckFolders();
		vector thepos = this.GetPosition();
		string pName = m_NoteContents.GetPlayer().GetIdentity().GetName();
		string pId = m_NoteContents.GetPlayer().GetIdentity().GetPlainId();
		SaveServerSide(pName , pId, m_NoteContents.GetNoteText()  , thepos[0].ToString()+" "+thepos[1].ToString()+" "+thepos[2].ToString());
		
	}

	
	void SaveServerSide(string PlayerName, string steam_id, string Notetext, string ps = "")
	{
		int hour;
		int minute;
		int second;
		int year;
		int month;
		int day;
		GetHourMinuteSecond(hour, minute, second);
		GetYearMonthDay(year, month, day);
		string suffix = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"_"+minute.ToString()+"_"+second.ToString();
		string m_ObjectFileName = "$profile:DZR/dzr_notes/log/"+steam_id+"_"+suffix+".txt";
		//FileHandle fhandle;
		if ( !FileExist(m_ObjectFileName) )
		{
			
			FileHandle fhandle = OpenFile(m_ObjectFileName, FileMode.WRITE);
			FPrintln(fhandle, Notetext );
			FPrintln(fhandle, "_________" );
			FPrintln(fhandle, PlayerName+" (steam ID: "+steam_id+")" );
			FPrintln(fhandle, this.ToString()+" ("+ps+")" );
			//FPrintln(fhandle, );
			CloseFile(fhandle);
			Print("DZR_PaperWritten.c ::: "+PlayerName + " ("+steam_id+") wrote text: " +Notetext+ " (logged to file: "+m_ObjectFileName+")");
		}
	}
	/*
		FileSerializer file = new FileSerializer;
		//FileHandle file = OpenFile(m_ObjectFileName, FileMode.APPEND);
		if (file.Open( m_ObjectFileName, FileMode.WRITE)
		{
		//file.Write(Notetext);ps
		file.Write(Notetext);
		file.Write(ps);
		file.Close();
		Print("DZR_PaperWritten.c ::: "+PlayerName + " ("+steam_id+") wrote text: " +Notetext+ " (logged to file: "+m_ObjectFileName+")");
		}
		else
		{
		Print("Failed write "+m_ObjectFileName);
		}
		} 
		
	*/
	void DZR_MissionServer_CheckFolders()
	{
		string dzr_Idz_ProfileFolder = "$profile:\\";
		string dzr_Idz_TagFolder = "DZR\\";
		string dzr_Idz_ModFolder = "dzr_notes\\";
		string dzr_Idz_SubFolder = "log\\";
		
		//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder)){
			//NO DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO DZR");
			MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder);	
			MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder);	
			//dzr_writeConfigFile();			
		}
		else 
		{
			//YES DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile YES DZR");
			if (!FileExist(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder)){
				//NO IDZ
				//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO IDZ");
				
				MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder);
				
				
				//dzr_writeConfigFile();
			}
			else 
			{
				if (!FileExist(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder+dzr_Idz_SubFolder)){
					MakeDirectory(dzr_Idz_ProfileFolder+dzr_Idz_TagFolder+dzr_Idz_ModFolder+dzr_Idz_SubFolder);
				}
			}
			
		};
		
		
		
		//PROPER CONFIG
		
	}
	
	override bool IsReadable()
	{
		return true;
	}
	
	override bool IsReadOnly()
	{
		return false;
	}
	
	override void SetActions()
	{
		AddAction(ActionPlaceObject);
		AddAction(ActionReadNoteOnGround);
		super.SetActions();
		AddAction(ActionWritePaper);
		AddAction(ActionReadPaper);
		AddAction(ActionTogglePlaceObject);
		
	}
	
	override bool DisableVicinityIcon()
	{
		return super.DisableVicinityIcon() + m_DZR_IsLocked;
	};
	
	void DZR_SetLocked(bool d_flag)
	{
		m_DZR_IsLocked = d_flag;
	};
	
	void DZR_SetItemNote(string dnote)
	{
		//m_DZR_ItemNote = dnote;
	};
	
	
	
    override void OnStoreSave( ParamsWriteContext ctx )
    {
		super.OnStoreSave( ctx );
        ctx.Write( m_DZR_IsLocked );
        //ctx.Write( m_DZR_ItemNote );

			//Print("===========Trying to SAVE locked state============");
			//Print(this);
			// Print("OnStoreSave m_DZR_IsLocked ("+m_DZR_IsLocked+") lifetime: "+GetLifetime());
			//Print("OnStoreSave m_DZR_ItemNote ("+m_DZR_ItemNote+")");
			//Print(ctx);
		
        
	}
	
	
    override void AfterStoreLoad() {
        super.AfterStoreLoad();
		
        if (GetGame().IsServer() && GetGame().IsMultiplayer()) {
				// Print("===========Trying to RESTORE locked state============");
				// Print(this);
				// Print("AfterStoreLoad m_DZR_IsLocked ("+m_DZR_IsLocked+") lifetime: "+GetLifetime());
			if (m_DZR_IsLocked)
			{
				SetLifetime(3888000);
				SetTakeable( false );
				//DZR_SetLocked(true);
				//DZR_SetItemNote(m_DZR_ItemNote);
				SetSynchDirty();
				// Print("AfterStoreLoad SetSynchDirty m_DZR_IsLocked ("+m_DZR_IsLocked+") NEW lifetime: "+GetLifetime());
				//Print("AfterStoreLoad SetSynchDirty m_DZR_ItemNote ("+m_DZR_ItemNote+")");
			}
		}
	}
	
	    override bool OnStoreLoad( ParamsReadContext ctx, int version )
    {
        if ( super.OnStoreLoad( ctx, version ) )
        {
            if ( !ctx.Read( m_DZR_IsLocked ) ) return false;

            return true;
        }

        return false;
    }
	
};

class DZR_AdminPen extends Pen_ColorBase
{

	override bool IsReadOnly()
	{
		return true;
	}
	
	override string Result()
	{
		return "DZR_PaperWrittenAdmin";
	}

};

class DZR_AdminPen_Red extends Pen_ColorBase
{

	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Red";
	}

};

class DZR_AdminPen_Green extends Pen_ColorBase
{

	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Green";
	}

};

class DZR_AdminPen_Blue extends Pen_ColorBase
{

	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Blue";
	}

};

//static
class DZR_AdminPen_Static extends Pen_ColorBase
{

	override bool IsReadOnly()
	{
		return true;
	}
	
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Static";
	}

};

class DZR_AdminPen_Red_Static extends DZR_AdminPen_Static
{

	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Red_Static";
	}

};

class DZR_AdminPen_Green_Static extends DZR_AdminPen_Static
{

	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Green_Static";
	}

};

class DZR_AdminPen_Blue_Static extends DZR_AdminPen_Static
{

	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Blue_Static";
	}

};

class DZR_PaperWrittenAdmin extends DZR_PaperWritten
{
	override bool IsReadOnly()
	{
		return true;
	}
	
	override void SetActions()
	{
		super.SetActions();
		RemoveAction(ActionWritePaper);
	}
}		

class DZR_PaperWrittenAdmin_Red extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Green extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Blue extends DZR_PaperWrittenAdmin
{
	
}
//static

class DZR_PaperWrittenAdmin_Static extends DZR_PaperWrittenAdmin
{
	
}

class DZR_PaperWrittenAdmin_Red_Static extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Green_Static extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Blue_Static extends DZR_PaperWrittenAdmin
{
	
}