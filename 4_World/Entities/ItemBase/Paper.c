modded class Paper
{
	
	bool IsReadable()
	{
		return false;
	}
	
	bool IsReadOnly()
	{
		return false;
	}
		
	void SetWrittenNoteText(string text)
	{
		m_NoteContents.SetNoteText(text);
	}
	
    override void SetActions()
	{
		//AddAction(ActionReadNoteOnGround);
		super.SetActions();
		AddAction(ActionWritePaper);
		//AddAction(ActionReadPaper);
		//AddAction(ActionTogglePlaceObject);
		//AddAction(ActionPlaceObject);
	}
}

modded class Pen_ColorBase
{
	bool IsReadOnly()
	{
		return false;
	}
	
	string Result()
	{
		return "DZR_PaperWritten";
	}
};

