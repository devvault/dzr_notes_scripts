/*
class dzrnotes_config_data_class
{
	//CONFIG
	string		Readme;
	int		RecognitionMode;
	int		ShowNameMode;
	int		MaskingMode;
	int		Crosshair;
	int		MaskingIndication;
	bool	AllowManualShow;
	bool	InstantManualShow;
	bool	ShowAliveName;
	bool	ShowDeadName;	
	//string	IdClassname;
	ref array<string> IdClassnames;
	//string	RecognitionKeyInput;
	//string	ManualNameShowKeyInput;
	ref array<string> FullMaskClassnames;
	float		FullMaskHide_m;
	ref array<string> HalfMaskClassnames;
	float		HalfMaskHide_m;
	ref array<string> HalfEyeMaskClassnames;
	float		HalfEyeMaskHide_m;
	ref array<string> GasMaskClassnames;
	float		GasMaskHide_m;
	ref array<string> ExcludedHeadgear;
	float	VisibleDistance_m;
	float	minVisibleDistance_m;
	float	DelayShow_ms;
	float	DelayHide_ms;
	float	NameUpdateFrequency_ms;
	float	DeadDistanceModifier_m;	
	int		NightMultiplier;
	int		NightModifier_m;
	int		OvercastModifier_m;
	float	FogModifier_m;
	bool	EnableDebug;
	string	Changelog;
	string	ModVersion;
	int		ConfigVersion;
	
	//ref array<string> text;
	ref DZRnotes_JsonSaveData text;
	//CONFIG
	
	//default
	
	//CONFIG
	
	void dzrnotes_config_data_class (){
		
		// Recognition logic
		Readme						= "Config is explained in Readme.txt in the Workshop mod folder";
		RecognitionMode				= 0; 
		ShowNameMode				= 1;
		MaskingMode					= 2;
		
		AllowManualShow				= true;
		InstantManualShow			= true; 
		ShowAliveName 				= true;
		ShowDeadName 				= true;
		
		// UI
		Crosshair					= 0; // 0 - OFF, 1 - Always ON, 2 - ON when in recognition mode.
		//CrosshairStyle				= 1; // 0 - Wide brackets, 1 - Green dot, 2 - White dot. (not implemented)
		MaskingIndication			= 1; // 0 - OFF, 1 - Always ON, 2 - ON when in recognition mode.
		// Indicators show how your face is masked:
		// 		Full white mask - your face is fully covered.
		// 		Half-cut transparent mask - you are wearing either glasses or a mask covering only half of your face.
		// 		Green mask - you activated AllowManualShow mode and anyone will see your name\face even if you wear mask.
		// 		Green Mask with ID - your ID is in hand or in the badge slot and it activates unrestricted name showing.
		
		// Timing (ms = milliseconds)		
		DelayShow_ms 				= 1500;	// Time before the name is shown when you looked at a player
		DelayHide_ms 				= 200;	// Time before the name is hidden when you look away from the player
		NameUpdateFrequency_ms 		= 500;	// How often you request target name from server 
		// !!!WARNING!!! Very small NameUpdateFrequency_ms value may reduce server and client performance, freeze game or even crash server;
		
		// Distances (m = meters)
		VisibleDistance_m 			= 20; 	// The name is never shown beyond that distance.
		DeadDistanceModifier_m 		= 6; 	// Subtract from VisibleDistance_m if target is dead.
		NightModifier_m 			= 8; 	// Subtract from VisibleDistance_m if the Sun is below the horizon.
		OvercastModifier_m 			= 2; 	// (Only at night) Multiplied by cloudiness intensity and is subtracted from VisibleDistance_m.
		FogModifier_m 				= 2; 	// (Only at night) Multiplied by fogginess intensity and is subtracted from VisibleDistance_m.
		NightMultiplier 			= 3; 	// Multiples (FogModifier_m + OvercastModifier_m).
		
		// Settings for MaskingMode = 2
		minVisibleDistance_m	 	= 0; 	// Use to allow everyone to see names withtin that distance.
		FullMaskHide_m 				= 22; 	// Subtract that from VisibleDistance_m if wearing a full mask 
		HalfMaskHide_m 				= 13; 	// Subtract that from VisibleDistance_m if wearing a full mask 
		HalfEyeMaskHide_m			= 8; 	// Subtract that from VisibleDistance_m if wearing a full mask 
		GasMaskHide_m 				= 15; 	// Subtract that from VisibleDistance_m if wearing a full mask 
		
		//IdClassname = "Pen_Black"; // What should act as ID in ShowNameMode = 2. Can be a base class.
		IdClassnames = new array<string >;
		IdClassnames.Insert("Paper");
		IdClassnames.Insert("ChernarusMap");
		
		
		
		// Define full masks.
		FullMaskClassnames = new array<string >;
		
		FullMaskClassnames.Insert("WeldingMask");
		FullMaskClassnames.Insert("DarkMotoHelmet_Black");
		FullMaskClassnames.Insert("DarkMotoHelmet_Blue");
		FullMaskClassnames.Insert("DarkMotoHelmet_Green");
		FullMaskClassnames.Insert("DarkMotoHelmet_Grey");
		FullMaskClassnames.Insert("DarkMotoHelmet_Lime");
		FullMaskClassnames.Insert("DarkMotoHelmet_Red");
		FullMaskClassnames.Insert("DarkMotoHelmet_White");
		
		FullMaskClassnames.Insert("Balaclava3Holes_Beige");
		FullMaskClassnames.Insert("Balaclava3Holes_Black");
		FullMaskClassnames.Insert("Balaclava3Holes_Blue");
		FullMaskClassnames.Insert("Balaclava3Holes_Green");
		
		FullMaskClassnames.Insert("GreatHelm");		
		
		// Define half masks. Will be full masks if MaskingMode = 1.
		HalfMaskClassnames = new array<string >;
		
		HalfMaskClassnames.Insert("BalaclavaMask_Beige");
		HalfMaskClassnames.Insert("BalaclavaMask_Black");
		HalfMaskClassnames.Insert("BalaclavaMask_Blackskull");
		HalfMaskClassnames.Insert("BalaclavaMask_Blue");
		HalfMaskClassnames.Insert("BalaclavaMask_Green");
		HalfMaskClassnames.Insert("BalaclavaMask_Pink");
		HalfMaskClassnames.Insert("BalaclavaMask_White");
		
		HalfMaskClassnames.Insert("Bandana_BlackPattern");
		HalfMaskClassnames.Insert("Bandana_Blue");
		HalfMaskClassnames.Insert("Bandana_CamoPattern");
		HalfMaskClassnames.Insert("Bandana_Greenpattern");
		HalfMaskClassnames.Insert("Bandana_Pink");
		HalfMaskClassnames.Insert("Bandana_PolkaPattern");
		HalfMaskClassnames.Insert("Bandana_RedPattern");
		HalfMaskClassnames.Insert("Bandana_Yellow");
		
		HalfMaskClassnames.Insert("SurgicalMask");
		HalfMaskClassnames.Insert("NioshFaceMask");
		HalfMaskClassnames.Insert("HockeyMask");
		HalfMaskClassnames.Insert("SantasBeard");
		
		HalfMaskClassnames.Insert("MotoHelmet_Black");
		HalfMaskClassnames.Insert("MotoHelmet_Blue");
		HalfMaskClassnames.Insert("MotoHelmet_Green");
		HalfMaskClassnames.Insert("MotoHelmet_Grey");
		HalfMaskClassnames.Insert("MotoHelmet_Lime");
		HalfMaskClassnames.Insert("MotoHelmet_Red");
		HalfMaskClassnames.Insert("MotoHelmet_White");
		
		// Define half masking glasses. Works only with MaskingMode = 2.
		HalfEyeMaskClassnames = new array<string >;
		HalfEyeMaskClassnames.Insert("AviatorGlasses");
		HalfEyeMaskClassnames.Insert("DesignerGlasses");
		HalfEyeMaskClassnames.Insert("SportGlasses_Black");
		HalfEyeMaskClassnames.Insert("SportGlasses_Blue");
		HalfEyeMaskClassnames.Insert("SportGlasses_Green");
		HalfEyeMaskClassnames.Insert("SportGlasses_Orange");
		
		// Define gasmasks. It helps to use gas masks but still be able to be recognized. Works only with MaskingMode = 2.
		GasMaskClassnames = new array<string >;
		GasMaskClassnames.Insert("GasMask");
		GasMaskClassnames.Insert("GP5GasMask");
		GasMaskClassnames.Insert("AirborneMask");
		
		//Exclusions for multislot items like a bandana, which can be a headgear and a mask.
		ExcludedHeadgear = new array<string >;
		ExcludedHeadgear.Insert("Bandana_BlackPattern");
		ExcludedHeadgear.Insert("Bandana_Blue");
		ExcludedHeadgear.Insert("Bandana_CamoPattern");
		ExcludedHeadgear.Insert("Bandana_Greenpattern");
		ExcludedHeadgear.Insert("Bandana_Pink");
		ExcludedHeadgear.Insert("Bandana_PolkaPattern");
		ExcludedHeadgear.Insert("Bandana_RedPattern");
		ExcludedHeadgear.Insert("Bandana_Yellow");
		
		// Internal
		EnableDebug = true; // Display diagnostic information to see how masking and distances change.
		Changelog = "Minor debug feature";
		ModVersion = "1.11.7"; // If the mod config is updated and your version is different, your config will be backed up and the new config will be applied. You can restore your settings from your backup file manually.
		ConfigVersion = 49; // If the mod config is updated and your version is different, your config will be backed up and the new config will be applied. You can restore your settings from your backup file manually.
		
		// ========== Version number explanation ===========
		// M.m.R.C as in 1.0.5.44
		//M = Major version, like complete overhaul or engine change.
		//m = Minor version, important or multiple feature addition
		//R = Revision, updated after some fixes or tweaks applied.
		//C = Config structure changed, it wil not be compatible with older configs, so you will have to upgrade your config using your backed up file.
		
		//CONFIG
		text = new DZRnotes_JsonSaveData();
		//text = new array< new array<string> >;
		//text.text.Insert("EMPTY1");
		
	}
	
}

modded class DayZGame {
	
	protected ref dzrnotes_config_data_class dzrnotes_config_data;
	
	dzrnotes_config_data_class ReadServerConfig() {
		if(dzrnotes_config_data){
			return dzrnotes_config_data;
		}
		return null;
	}
	
	void SaveConfigOnServer(ref dzrnotes_config_data_class new_config) {
		//Print("[DZR IdentityZ] ::: DayZGame ::: SaveConfigOnServer");
		dzrnotes_config_data = new_config;
		//Print("[DZR IdentityZ] ::: DayZGame ::: SaveConfigOnServer");
	}
}
*/
class DZRnotes_JsonSaveData
{
	ref array<string> text;
	
	void DZRnotes_JsonSaveData()
	{
		text = new array<string >;
	}
}

class DZR_Notes
{
	array<string> PackText(string text)
	{
		array<string> strs = new array<string>;
		string strSub;
		//Print("Start packing:" + text);
		int BufferSize = 256;
		int strLen = text.LengthUtf8();
		//Print("text.LengthUtf8()" + strLen);
		int strStart = 0;
		int strEnd = BufferSize; //256
		if(strLen > BufferSize)
		{
			while (strLen >= BufferSize)
			{
				strSub = text.SubstringUtf8(strStart, BufferSize); //0-256
				strLen = strSub.LengthUtf8(); // 256
				//Print("Adding string from "+strStart+" to "+strEnd+" ("+strLen+" ch): " + strSub);
				strStart = strStart + BufferSize; // 256
				strEnd = strEnd + BufferSize; //512
				strs.Insert(strSub);
			}
		}
		else
		{
			strs.Insert(text);
		}
		return(strs);
	}
	
	static string UnpackText(array<string> text)
	{
		string uncompressed;
		for ( int i = 0; i < text.Count(); i++ )
		{
			uncompressed += text.Get(i)+"\n";
		}
		return(uncompressed);
	}
	
	array<string> UnpackTextToArray(array<string> text, bool EnableDebug = false)
	{
		if(EnableDebug) {Print("Unpacking to array");};
		
		array<string> strs = new array<string>;
		for ( int i = 0; i < text.Count(); i++ )
		{
			strs.Insert(text.Get(i));
			if(EnableDebug) {Print("Unpacking : Inserting "+text.Get(i));};
		}
		
		if(EnableDebug) {Print("Returning");};
		return(strs);
	}
	
	string SubstringUtf8Inverted( string string_to_split, int position_start, int position_end )
	{
		string first_half = string_to_split.SubstringUtf8(0, position_start);
		string second_half = string_to_split.SubstringUtf8( position_end, string_to_split.LengthUtf8() - position_end );
		string result = first_half + second_half;
		return result;
	}
}

